CFLAGS = -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wswitch-enum \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunsafe-loop-optimizations \
	  -Wunused-but-set-variable \
	  -Wwrite-strings

# for usleep()
CFLAGS += -D_BSD_SOURCE

# for clock_gettime()
CFLAGS += -D_POSIX_C_SOURCE=199309L

LDLIBS += -lpthread -lrt

double-buffering: double-buffering.o

clean:
	rm -rf *~ *.o *.jpg double-buffering

test: double-buffering
	valgrind --leak-check=full --show-reachable=yes ./double-buffering
